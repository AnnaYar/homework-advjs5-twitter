/*Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

Технічні вимоги:

При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. 
Для цього потрібно надіслати GET запит на наступні дві адреси:
https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts
Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад доданий в репозиторії групи, посилання на ДЗ вище), 
та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. 
При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. 
Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. 
Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. 
При необхідності ви можете додавати також інші класи.

Необов'язкове завдання підвищеної складності
Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. 
Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, 
в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно 
надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху 
сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно 
надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.*/


async function loadUsers() {
  try {
    const response = await fetch("https://ajax.test-danit.com/api/json/users");
    if (!response.ok) {
      throw new Error('Помилка при отриманні користувачів');
    }
    const users = await response.json();
    console.log(users);

    // Завантаження та відображення користувачів
    loadPosts(users);
  } catch (error) {
    console.log(error, 'Помилка при отриманні користувачів');
  }
}

// Завантаження та відображення публікацій
async function loadPosts(users) {
  try {
    const response = await fetch("https://ajax.test-danit.com/api/json/posts");
    if (!response.ok) {
      throw new Error('Помилка при отриманні публікацій');
    }
    const posts = await response.json();
    console.log(posts);

    posts.forEach(post => {
      const user = users.find(user => user.id === post.userId);
      const card = new Card(post, user);
      card.createCard();
    });
  } catch (error) {
    console.log(error, 'Помилка при отриманні публікацій');
  }
}

loadUsers();

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
  }

createCard() {
  const cardContainer = document.getElementById('cards');
  const cardElement = document.createElement('div');
  cardElement.classList.add('card');
  cardElement.setAttribute('id', `card-${this.post.id}`);



  cardElement.innerHTML = `<p class="user-name">${this.user.name} ${this.user.username}  <span>${this.user.email}</span></p>
  <h2 class="title">${this.post.title}</h2>
  <p class="text">${this.post.body}</p>
  <button class="delete-button">Delete</button>`;

  const deleteButton = cardElement.querySelector('.delete-button');
  deleteButton.addEventListener('click', () => this.deleteCard());

  cardContainer.appendChild(cardElement);
}

deleteCard(){
  fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
    method: 'DELETE',
  })
    .then(response => {
      if (response.ok) {
        const cardElement = document.getElementById(`card-${this.post.id}`);
        cardElement.remove();
      } else {
        console.error('Помилка при видаленні карти');
      }
    })
    .catch(error => console.error('Помилка при видаленні карти:', error));
}
}

